// Package httpclient serves as a client wrapper for the ezApi.
package httpclient

import (
	"net/http"

	"github.com/cockroachdb/errors"
)

// HTTPClient defines the methods required for an HTTP client.
type HTTPClient interface {
	GetBaseURL() string
	GetAPIKey() *string
	GetHTTP() http.Client
	Do(r *http.Request) (*http.Response, error)
}

// Client implements HTTPClient.
type Client struct {
	http    http.Client
	baseURL string
	apiKey  *string
}

// Config defines the config for the HTTPClient of the energy zero api.
type Config struct {
	BaseURL string
	APIKey  string
}

// NewClient returns a new HTTP client.
func NewClient(http http.Client, baseURL string, apiKey *string) *Client {
	return &Client{
		http:    http,
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

// GetBaseURL gets the baseURL for energy zero.
func (c *Client) GetBaseURL() string {
	return c.baseURL
}

// GetHTTP gets the client for energy zero.
func (c *Client) GetHTTP() http.Client {
	return c.http
}

// GetAPIKey gets the Api key for the client.
func (c *Client) GetAPIKey() *string {
	return c.apiKey
}

// Do makes the httprequest for energy zero.
func (c *Client) Do(r *http.Request) (*http.Response, error) {
	response, err := c.http.Do(r)
	if err != nil {
		return nil, errors.Wrap(err, "doing http request")
	}

	return response, nil
}
