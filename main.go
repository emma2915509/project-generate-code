package main

import (
	"context"
	"fmt"
	"net/http"
	"project-generate-code/pkg/httpclient"
)

func main() {
	fmt.Println("Hello World.")

	token := "glpat-JtpEt3fFDex5Z5wz11y-"

	client := httpclient.NewClient(http.Client{}, "gitlab.com/emma2915509/project-generate-code", &token)

	requestURL := fmt.Sprintf("https://%v%v", client.GetBaseURL(), "/api/v4/issues")

	ctx := context.Background()

	request, err := http.NewRequestWithContext(ctx, "GET", requestURL, nil)

	request.Header.Set("PRIVATE-TOKEN", token)

	fmt.Println("request", request)

	response, err := client.Do(request)

	fmt.Println("err 2", err)

	defer func() {
		err = response.Body.Close()
	}()

	fmt.Println("err", err)

	// responseBody, err := io.ReadAll(response.Body)

}
